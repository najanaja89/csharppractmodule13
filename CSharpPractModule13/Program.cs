﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpPractModule13
{
    class Program
    {
        static void Main(string[] args)
        {
            Random random = new Random();
            List<int> arrayList = new List<int>()
            {
               random.Next(1,100),
               random.Next(1,100),
               random.Next(1,100),
               random.Next(1,100),
               random.Next(1,100),
               random.Next(1,100),
               random.Next(1,100),
               random.Next(1,100),
               random.Next(1,100),
               random.Next(1,100),
               random.Next(1,100),
               random.Next(1,100)
            };

            foreach (var item in arrayList)
            {
                Console.Write(item + "\t");
            }
            Console.WriteLine();

            Console.WriteLine("Average value is " + arrayList.Average());
            Console.WriteLine("All elements, where value more than average ");
            foreach (var item in arrayList)
            {
                if (item > arrayList.Average())
                {
                    Console.Write(item + "\t"); ;
                }
            }
            Console.WriteLine();

            Console.WriteLine("All odd elements ");
            for (int i = 0; i < arrayList.Count; i++)
            {
                if (((double)arrayList[i] % 2) != 0)
                {
                    arrayList.RemoveAt(i);
                    i--;
                }
            }

            foreach (var item in arrayList)
            {
                Console.Write(item + "\t");
            }
            Console.WriteLine();

            Console.ReadLine();
        }
    }
}
